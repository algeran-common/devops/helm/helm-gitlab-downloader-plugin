package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

var (
	client = &http.Client{}
)

type GitlabResponse struct {
	Content string
}

func main() {
	repoArguments := os.Args
	var fullUrlPath string

	for _, arg := range repoArguments {
		if arg != "" && strings.HasPrefix(arg, "gitlab://") {
			fullUrlPath = arg
		}
	}
	if fullUrlPath == "" {
		log.Fatal("arguments does not contain repo path or it is invalid scheme. Supported only 'gitlab'")
	}

	err := printRepoContent(fullUrlPath)

	if err != nil {
		log.Fatal(err)
	}
}

func printRepoContent(fullUrlPath string) (err error) {
	parsedUrl, err := parseUrl(fullUrlPath)
	if err != nil {
		return err
	}

	getFileUrl := constructGetFileUrl(parsedUrl)

	jsonBody, err := getGitlabRepoContent(getFileUrl)
	if err != nil {
		return err
	}

	decodedContent, err := base64.StdEncoding.DecodeString(jsonBody.Content)
	if err != nil {
		return errors.Wrap(err, "error on decode content from base64")
	}

	_, err = os.Stdout.Write(decodedContent)
	if err != nil {
		return errors.Wrap(err, "error on writing content to stdout")
	}

	return nil
}

func getGitlabRepoContent(getFileUrl string) (jsonBody *GitlabResponse, err error) {
	request, err := http.NewRequest("GET", getFileUrl, nil)

	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("error on creating request with URL %s", getFileUrl))
	}

	gitlabPrivateToken := os.Getenv("GITLAB_TOKEN")
	if gitlabPrivateToken != "" {
		request.Header.Add("PRIVATE-TOKEN", gitlabPrivateToken)
	}

	response, err := client.Do(request)
	if err != nil {
		return nil, errors.Wrap(err, "error on call Gitlab API")
	}

	if response.StatusCode != http.StatusOK {
		response.Body.Close()
		return nil, errors.Errorf("Gitlab return %d status code on call %s", response.StatusCode, getFileUrl)
	}

	defer response.Body.Close()
	jsonBody = &GitlabResponse{}
	err = json.NewDecoder(response.Body).Decode(jsonBody)
	if err != nil {
		return nil, errors.Wrap(err, "Error on reading JSON from response")
	}
	return jsonBody, nil
}

func constructGetFileUrl(parsedUrl *url.URL) (combinedUrl string) {
	projectId, filePath := splitProjectIdAndFilePath(parsedUrl.Path)

	combinedUrl = fmt.Sprintf(
		"https://%s/api/v4/projects/%d/repository/files/%s?ref=master",
		parsedUrl.Host,
		projectId,
		url.PathEscape(filePath))
	return
}

func splitProjectIdAndFilePath(path string) (projectId int, filePath string) {
	splitPath := strings.Split(path, "/")
	if cap(splitPath) < 3 { // first element is always an empty string
		log.Fatal("path should contain at least 2 parameters - projectId and filePath")
	}

	projectIdString := splitPath[1]
	projectId, err := strconv.Atoi(projectIdString)
	if err != nil {
		log.Fatalf("second path param is not a numeric identifier of Gitlab project %s", projectIdString)
	}

	filePath = strings.Join(splitPath[2:], "/")
	return
}

func parseUrl(unparsedUrl string) (parsedUrl *url.URL, err error) {
	parsedUrl, err = url.Parse(unparsedUrl)

	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("error on parse url %s", unparsedUrl))
	}
	return
}
