# HELM GITLAB DOWNLOADER PLUGIN

This plugin designed to interact with Gitlab repository as helm repository. Used Gitlab API v4.  
Repo path should be specified with `gitlab` schema, gitlab server address and project id of gitlab repository
that will be used as helm repo.

Example:
`gitlab://gitlab.server.com/1234`

## Requirements
1. If gitlab repository (helm repo) is private, so you need to specify environment variable 
    `GITLAB_TOKEN` with GitLab token as value. `api` scope required.
2. Repository of helm will use sources only from `master` branch
    
## How to use

### Prepare helm-gitlab repository

1. In your chart repository build artefact
    ```
      helm package <chart_name> .
    ```
   That command will generate an archive with you chart `chart_name.tgz`

2. Place generated archive `chart_name.tgz` to gitlab repository that will be used as helm repo.
    You can make any directories and layers inside as you wish.

3. In helm-gitlab repository run next command to create `index.yaml` that will be used by the helm to observe a repo
    (you need to specify your gitlab server address and project id (like `1234`):
    ```
      helm repo indexß --url=gitlab://gitlab.server.com:<project_id> .
    ``` 
   
   Or, if you already have `index.yaml` in helm-gitlab repo, you can use merge argument:
   ```
     helm repo index --merge=index.yaml --url=gitlab://gitlab.server.com:<project_id> .
   ```

4. Push `index.yaml` file and chart archives to gitlab

### Configure helm

1. Clone this repo

2. build plugin binary with:
    ```
        go build -o helm-gitlab-downloader-plugin
    ```
   
3. Install the plugin to helm:
    ```
        helm plugin install .
    ```

4. Add a repository to helm:
    ```
       helm repo add repo_name gitlab://gitlab.server.com:<project_id>
    ``` 

5. Enjoy :)
